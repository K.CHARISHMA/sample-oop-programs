#include<iostream>
using namespace std;
class partialAbstract{
   public:
       void fruit(){
           cout<<"Fruits are healthy"<<endl;
       }
       virtual void vegetable()=0;
};
class child : public partialAbstract{
   public:
       void vegetable(){
           cout<<"Vegetables are mostly green in colour"<<endl;
       }
};
int main(){
   child obj;
   obj.fruit();
   obj.vegetable();
}
