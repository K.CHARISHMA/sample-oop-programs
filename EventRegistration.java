import java.awt.*;
import java.awt.event.*;
public class EventRegistrationForm extends Frame implements ActionListener {
    private Label titleLabel, nameLabel, emailLabel, phoneLabel, registerLabel;
    private TextField nameField, emailField, phoneField;
    private CheckboxGroup registerOptions;
    private Checkbox yesCheckbox, noCheckbox;
    private Button submitButton;
    public EventRegistrationForm() {
        setLayout(new GridLayout(6,2));
        titleLabel = new Label("Event Registration Form");
        add(titleLabel);
        add(new Label(""));
        nameLabel = new Label("Name:");
        add(nameLabel);
        nameField = new TextField(20);
        add(nameField);
        emailLabel = new Label("Email:");
        add(emailLabel);
        emailField = new TextField(20);
        add(emailField);
        phoneLabel = new Label("Phone:");
        add(phoneLabel);
        phoneField = new TextField(20);
        add(phoneField);
        registerLabel = new Label("Will you attend the event?");
        add(registerLabel);
        registerOptions = new CheckboxGroup();
        yesCheckbox = new Checkbox("Yes", registerOptions, true);
        noCheckbox = new Checkbox("No", registerOptions, false);
        add(yesCheckbox);
        add(noCheckbox);
        submitButton = new Button("Submit");
        add(submitButton);
        submitButton.addActionListener(this);
        setTitle("Event Registration Form");
        setSize(300, 200);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == submitButton) {
            String name = nameField.getText();
            String email = emailField.getText();
            String phone = phoneField.getText();
            String register = registerOptions.getSelectedCheckbox().getLabel();
            System.out.println("Name: " + name);
            System.out.println("Email: " + email);
            System.out.println("Phone: " + phone);
            System.out.println("Attending: " + register);
        }
    }
        public static void main(String[] args) {
        new EventRegistrationForm();
        }
}
