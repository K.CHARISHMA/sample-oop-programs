#include<iostream>
using namespace std;


class Box{
   //float length,width,height;
   public:
  
   //member method inside class
       void boxArea(float length,float width,float height){
          cout<<"area :"<<2*(length*width+width*height+height*length)<<endl;
       }
       //member function outside function:boxVolume
       void boxVolume(float length,float width,float height);
       friend void displayBoxDimensions(Box);
       inline void displayWelcomeMessage();
       inline void displayExitMessage(){
           cout<<"Exit"<<endl;
       }


      
};
void Box:: boxVolume(float length,float width,float height){
   cout<<"volume: "<<length*width*height<<endl;
}
void displayBoxDimensions(Box obj){
   cout<<"The dimensions are : "<<endl;
}
inline void Box:: displayWelcomeMessage(){
     cout<<"Welcome to program"<<endl;
}
int main(){
   float length=10,width=20,height=30;


   Box obj;
   obj.boxArea(length,width,height);
   obj.boxVolume(length,width,height);
   displayBoxDimensions(obj);
   obj.displayWelcomeMessage();
   obj.displayExitMessage();
}
