import java.lang.*;
import java.util.*;
public class Collection{
    public static void main(String[] args) {
        Stack<Integer> obj = new Stack<Integer>();
        System.out.println(obj.empty());
        obj.push(10);
        obj.push(20);
        obj.push(30);
        obj.pop();
        System.out.println("position of 20 is : "+obj.search(20));
        System.out.println("peek element in stack is : "+obj.peek());
        System.out.println("Stack is : "+obj);


        System.out.println("\n");
        Queue<Double> obj1= new PriorityQueue<Double>();
        obj1.add(1.1);         //offer(1.1)
        obj1.add(2.1);
        obj1.add(3.1);
        obj1.add(4.1);
        obj1.remove();         //poll()
        System.out.println("First element in queue is : "+obj1.element());  //peek()
        System.out.println("Queue is : "+obj1);
    }
}
