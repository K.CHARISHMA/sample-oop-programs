import java.lang.*;
class MethOverride{
    void sub(int a,int b){
        System.out.println("Substraction of 2 integer numbers is "+(a-b));
    }
    void sub(double a,double b){
        System.out.println("Substraction of 2 floating numbers is "+(a-b));
    }


    public static void main(String[] args){
        MethOverride obj=new MethOverride();
        obj.sub(10,20);
        obj.sub(30.5,20.0);
       
    }
}
