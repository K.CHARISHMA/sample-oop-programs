public class Boxing1 {
   
    public static void main(String[] args) {
        //boxing
        Integer num1 = new Integer(10);
        System.out.println("num1 is "+num1);
        //unboxing
        int num2 = num1;
        System.out.println("num2 is "+num2);
       
        Character alp1 = new Character('c'); //boxing
        System.out.println("char1 is "+alp1);
        char alp2 = alp1;                     //unboxing
        System.out.println("char2 is "+alp2);


        Float a = new Float(2.5);
        System.out.println("Float1 is "+ a);
        float b= a+3;
        System.out.println("Float2 is "+b);
    }
}
