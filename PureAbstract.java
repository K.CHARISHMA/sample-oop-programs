import java.lang.*;
interface dog{
    public void bark();
}
interface cat{
    public void meow();
}
class PureAbstract implements dog,cat{
    public void bark(){
        System.out.println("Dogs always bark");
    }
    public void meow(){
        System.out.println("cats are meowing");
    }
    public static void main(String[] args) {
        PureAbstract obj =new PureAbstract();
        obj.bark();
        obj.meow();
    }
}
