#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct emp {
    int id, age;
    float sal;
    char name[20], dept[20];
};


int main() {
    struct emp s[3];


    FILE *fp = fopen("EmployeeDetails.txt", "w");
    for (int i = 0; i < 3; i++) {
        printf("Enter record of employee details in MVGR college:\n\n");
        printf("\nEnter name of employee: ");
        scanf("%s", s[i].name);
        printf("\nEnter department of employee : ");
        scanf("%s", s[i].dept);
        printf("\nEnter employee id number : ");
        scanf("%d", &s[i].id);
        printf("\nEnter the salary of employee : ");
        scanf("%f", &s[i].sal);
        printf("\nEnter the age of employee : ");
        scanf("%d", &s[i].age);


        fprintf(fp, "Employee Name:%s\nDepartment Name:%s\nId number:%d\nSalary:%f\nAge:%d\n\n",
                s[i].name, s[i].dept, s[i].id, s[i].sal, s[i].age);
        printf("\nRecord stored in file...\n");
    }


    fclose(fp);


    fp = fopen("EmployeeDetails.txt", "r");
    char line[100];
    printf("\nRetrieved names from the file:\n");
    while (fgets(line, sizeof(line), fp) != NULL) {
        if (strstr(line, "Employee Name:") != NULL) {
           
            char *nameStart = strchr(line, ':') + 1;
            char *nameEnd = strchr(nameStart, '\n');
            if (nameEnd != NULL) {
                *nameEnd = '\0';
            }
            printf("%s\n", nameStart);
        }
    }


    fclose(fp);
    return 0;
}
