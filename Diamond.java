interface A{
    public  abstract void display1();
}
interface B{
    public abstract void display2();
}
 class Diamond implements A,B{
    public void display1(){
        System.out.println("Welcome");

    }
    public void display2(){
        System.out.println("Hi");
    }
    public static void main(String[] args){
        Diamond obj=new Diamond();
        obj.display1();
        obj.display2();
    }
}
