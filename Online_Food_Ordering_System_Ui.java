import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
class Login extends JFrame{
	JTextField t1,t2;
	JButton b1,b2;
	JLabel l1,l2,l3,l4;
	Login(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		l1 = new JLabel("LOGIN");
		l1.setFont(new Font("Times new Roman",Font.BOLD,30));
		l1.setForeground(Color.BLUE);
		l1.setBounds(100,10,300,30);
		add(l1);
		t1 = new JTextField(60);
		t2 = new JPasswordField(60);
		b1 = new JButton("SignIn");
		b2 = new JButton("SignUp");
		t1.setBounds(100,60,120,30);
		t2.setBounds(100,100,120,30);
		b1.setBounds(120,140,80,30);
		b2.setBounds(120,170,80,30);
		l2 = new JLabel("");
		l2.setBounds(250,80,300,30);
		l3 = new JLabel("UserName");
		l3.setBounds(20,60,70,30);
		add(l3);
		l4 = new JLabel("Password");
		l4.setBounds(20,100,70,30);
		add(l4);
		add(l2);
		add(t1);
		add(t2);
		add(b1);
		add(b2);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				boolean match = false;
				String uname = t1.getText().toString();
				String pwd = t2.getText().toString();
				try{
					FileReader fr = new FileReader("login.txt");
					BufferedReader br = new BufferedReader(fr);
					String str;
					while((str=br.readLine())!=null){
						if(str.equals(uname+"\t"+pwd)){
							match = true;
							break;
						}
					}
					fr.close();
				}catch(Exception e){}
				if(match){
					dispose();
					Page1 p1 = new Page1();
					p1.setBounds(400,200,450,300);
					p1.setVisible(true);
				}
				else{
					l2.setText("Invalid Username or Password!!");
				}
			}
				
		});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				SignUp s = new SignUp();
				s.setVisible(true);
				s.setBounds(200,200,500,400);
			}
		});
		
	}
}
class SignUp extends JFrame{
	JTextField t1,t2,t3,t4;
	JLabel l1,l3,l4,l2,l5;
	JButton b1,b2;
	SignUp(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		l1 = new JLabel("SignUp");
		l1.setFont(new Font("Times new Roman",Font.BOLD,20));
		l1.setForeground(Color.BLUE);
		l1.setBounds(40,10,100,30);
		add(l1);
		t1 = new JTextField(60);
		t2 = new JTextField(60);
		t3 = new JTextField(60);
		t4 = new JTextField(60);
		b1 = new JButton("Submit");
		b2 = new JButton("Cancel");
		t1.setBounds(100,50,80,30);
		t2.setBounds(100,90,80,30);
		t3.setBounds(100,130,80,30);
		l2 = new JLabel("Mobile No.");
		l2.setBounds(20,130,80,30);
		add(l2);
		add(t3);
		add(t4);
		l3 = new JLabel("UserName");
		l3.setBounds(20,50,70,30);
		add(l3);
		l4 = new JLabel("Password");
		l4.setBounds(20,90,70,30);
		add(l4);
		b1.setBounds(100,170,80,30);
		b2.setBounds(100,210,80,30);
		add(t1);
		add(t2);
		add(b1);
		add(b2);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw = new FileWriter("login.txt",true);
					fw.write(t1.getText()+"\t"+t2.getText()+"\n");
					fw.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Registration Completed");
					dispose();
				}catch(Exception e){}
			}
		}
		});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Login l = new Login();
				l.setBounds(400,200,400,300);
				l.setVisible(true);	
			}
		});
	}
}
class Page1 extends JFrame{
	JLabel l1,l2,l3;
	JComboBox<String> restaurantComboBox, foodComboBox;
	ArrayList<String> GrandOptions,EatersOptions;
    JTextField t1;
	JButton b1,b2,b3,b4;
	String selectedItem;
	Page1(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Restaurant Order System");
		l1 = new JLabel("Select Restaurant");
		l2 = new JLabel("Select Food");
		l3 = new JLabel("Enter Quantity");
		t1 = new JTextField(60);
		t1.setBounds(170,110,150,30);
		l1.setBounds(10,30,150,30);
		l2.setBounds(10,70,150,30);
		l3.setBounds(10,110,150,30);
		add(l1);
		add(l2);
		add(l3);
		add(t1);
		restaurantComboBox = new JComboBox<String>(new String[] {"Grand Hotel Alpha", "Eaters Stop"});
		restaurantComboBox.setBounds(170,30,120,30);
		add(restaurantComboBox);
		foodComboBox = new JComboBox<String>();
		foodComboBox.setBounds(170,70,200,30);
		add(foodComboBox);
		b1 = new JButton("Confirm");
		b1.setBounds(290,30,100,30);
		add(b1);
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				File file = new File("orders.txt");
			try { if (file.exists()) {
					if (file.length() != 0) {
                    JOptionPane.showMessageDialog(null,"Payment is Not Done","ERROR",JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,"Please Clear the Order","ERROR",JOptionPane.ERROR_MESSAGE);
                }
            } else {
					selectedItem = (String) restaurantComboBox.getSelectedItem();
				if(selectedItem.equals("Grand Hotel Alpha")){
					foodComboBox.removeAllItems();
					GrandOptions = new ArrayList<String>();
					GrandOptions.add("Fried Rice - 180");
					GrandOptions.add("Biryani - 220");
					GrandOptions.add("Paneer Curry - 150");
					GrandOptions.add("Meals - 170");
					GrandOptions.add("Chicken Fry Biryani - 220");
					GrandOptions.add("Thandoori Chicken - 250");
					GrandOptions.add("Mutton Curry - 300");
					GrandOptions.add("Fish Fry - 180");
					GrandOptions.add("Samosa - 10");
					GrandOptions.add("French Fries - 150");
					GrandOptions.add("Sandwiches - 120");
					GrandOptions.add("Popcorn - 30");
                for (String option : GrandOptions) {
					foodComboBox.addItem(option);
                }
				}
				else if(selectedItem.equals("Eaters Stop")){
					foodComboBox.removeAllItems();
					EatersOptions = new ArrayList<String>();
					EatersOptions.add("Fried Rice - 150");
					EatersOptions.add( "Chicken Noodle Soup - 120");
					EatersOptions.add("Grilled Chicken - 200");
					EatersOptions.add("Spaghetti Bolognese-180");
					EatersOptions.add("Club Sandwich - 90");
					EatersOptions.add("Margherita Pizza - 200");
					EatersOptions.add("Beef Burger - 120");
					EatersOptions.add("ChickenFajita-150");
                for (String option : EatersOptions) {
					foodComboBox.addItem(option);}
					}
					selectedItem="";
				}
				} catch (Exception ex) {}
				
			}
		});
		b2 = new JButton("Add Item");
		b2.setBounds(40,150,120,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String comboBoxSelection = (String) foodComboBox.getSelectedItem();
				String textFieldText = t1.getText();
				String cost = comboBoxSelection.substring(comboBoxSelection.length() - 3);
				int pri = (Integer.parseInt(cost)*Integer.parseInt(textFieldText));
				String price = String.valueOf(pri);
				if(t1.getText().toString().isEmpty()||comboBoxSelection.isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
					try{
					FileWriter fw = new FileWriter("orders.txt",true);
					fw.write(comboBoxSelection+","+textFieldText+"\n");
					fw.close();
					FileWriter fw1 = new FileWriter("price.txt",true);
					fw1.write(price+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f," Item is Added Successfully");
				}catch(Exception ex){}
				t1.setText("");
			}
			}
		});
		b3 = new JButton("Place Order");
		b3.setBounds(165,150,120,30);
		add(b3);
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Placeorder po = new Placeorder();
				po.setBounds(300,50,400,400);
				po.setVisible(true);
			}
		});
	}
}
class Placeorder extends JFrame{
	JPanel p1;
	JButton b1,b2;
	JLabel l1;
	JTextField t1;
	ArrayList<String[]> data = new ArrayList<>();
	JTable table;
	DefaultTableModel model;
	JScrollPane sp;
	int m=0,m1=0;
	Placeorder(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Orders Placed");
		p1 = new JPanel();
		p1.setBounds(10,80,400,300);
		add(p1);
		try {		
            BufferedReader reader = new BufferedReader(new FileReader("orders.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] row = line.split(",");
                data.add(row);
            }
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        model = new DefaultTableModel(new String[]{"Item", "Quanlity"}, 0);
        for (String[] row : data) {
            model.addRow(row);
        }
        table = new JTable(model);
		sp = new JScrollPane(table);
        sp.setPreferredSize(new Dimension(350, 250));
		p1.add(sp);
		b1 = new JButton("<--Back");
		b1.setBounds(5,20,100,20);
		add(b1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Page1 p1 = new Page1();
					p1.setBounds(400,200,450,300);
					p1.setVisible(true);
			}
		});
		try{	FileReader fr = new FileReader("price.txt");
				BufferedReader br = new BufferedReader(fr);
				String str;
				while((str=br.readLine())!=null){
				m += Integer.parseInt(str);}
				fr.close();
				br.close();
			}catch(Exception e){}
		String s1=String.valueOf(m);
		l1 = new JLabel("Bill :"+ s1);
		l1.setBounds(50,50,100,30);
		add(l1);
		b2 = new JButton("Payment");
		b2.setBounds(150,30,100,30);		
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				Scanner scn = new Scanner(System.in);
				File file = new File("price.txt");
				File file1 = new File("orders.txt");
        try {
            if (file.exists()&&file1.exists()) {
				try{	FileReader fr = new FileReader("price.txt");
				BufferedReader br = new BufferedReader(fr);
				String str;
				while((str=br.readLine())!=null){
				m1 += Integer.parseInt(str);}
				fr.close();
				br.close();
			}catch(Exception e){}
				System.out.println("Your Bill is "+m1);
				System.out.println("Do you Pay (Y/N): ");
				if (scn.next().equals("Y")){
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Payment Completed");
					file1.delete();
					file.delete();
					scn.close();
					JOptionPane.showMessageDialog(f,"Thank You for Ordering");
					m1=0;
					dispose();
					Page1 p1 = new Page1();
					p1.setBounds(400,200,450,300);
					p1.setVisible(true);
					}
					else{JOptionPane.showMessageDialog(null,"Your can not make another Order","ERROR",JOptionPane.ERROR_MESSAGE);}
            } else {
                JOptionPane.showMessageDialog(null,"Please Make An Order","ERROR",JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {}
			}
		});
	}
}
public class Order {
	public static void main(String[] args){
		Login l = new Login();
				l.setBounds(400,200,300,250);
				l.setVisible(true);	
	}
}
