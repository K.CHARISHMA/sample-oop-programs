

#include<iostream>
using namespace std;
class Demo1{
   public:
       int a=10;
       void demo1Msg(){
           cout<<"Demo 1 class\n";
       }
};
//simple inheritance
class Demo2 : public Demo1{
   public:
       int b=20;
       void demo2Msg(){
       cout<<"Demo 2 class\n"<<a+b<<endl;
       }
};
//simple inheritance
class Demo3:public Demo1{
   public:
       int c=30;
       void demo3Msg(){
       cout<<"Demo 3 class\n"<<a+c<<endl;
       }
};
//Multiple inheritance
class Demo4: public Demo2,public Demo3{
   public:
       int d=40;
       void demo4Msg(){
       cout<<"Demo 4 class\n"<<b+c+d<<endl;
       }
};
int main(){
Demo4 obj;
cout<<"Diamond inheritance\n";
//cout<<obj.a<<endl;//ambiguous
//obj.demo1Msg();//ambiguous
obj.demo2Msg();
obj.demo3Msg();
obj.demo4Msg();
}
