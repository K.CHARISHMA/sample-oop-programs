import java.util.*;


public class Data{
    public static void main(String[]args) {
        Scanner obj=new Scanner(System.in);
        System.out.println("Enter a name : ");
        String name=obj.next();
        boolean a=isString(name);
        if(a){
            System.out.println("Hello "+name);
        }
        else{
            System.out.println("Invalid username");
        }
    }
    static boolean isString(String n){
        int i;
        for( i=0;i<n.length();i++){
            if(n.charAt(i)<48 || n.charAt(i)>57){
                return true;
            }
        }
            return false;
        }
}
