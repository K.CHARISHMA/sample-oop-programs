import java.awt.*;
import java.awt.event.*;
public class Ui implements ActionListener{
    Ui(){
        Frame fr =new Frame();
        fr.setTitle("LOGIN PAGE");
        fr.setBackground(Color.GRAY);
        fr.setBounds(0,0,200,200);
        fr.setLayout(new FlowLayout());
        Label title = new Label("LOGIN FORM");
        title.setBounds(0,0,10,20);
        Label unameLabel = new Label("NAME");
        unameLabel.setBounds(1,0,10,20);
        TextField unameBox = new TextField(20);
        unameBox.setBounds(1,1,10,10);
        Label PassLabel = new Label("PASSWORD");
        PassLabel.setBounds(2,0,10,20);
        TextField passBox = new TextField(20);
        passBox.setBounds(2,1,10,20);
        Button submit = new Button("LOGIN");
        submit.setBounds(3,0,20,20);
        TextField msg= new TextField(20);
       
        fr.add(title);
        fr.add(unameLabel);
        fr.add(unameBox);
        fr.add(PassLabel);
        fr.add(passBox);
        fr.add(submit);
        fr.setVisible(true);
        fr.add(msg);
     
        fr.addWindowListener(
        new WindowAdapter() {
         public void windowClosing(WindowEvent obj){
            fr.dispose();
         }  
        }
      );
      fr.addMouseListener(
        new MouseAdapter(){
            public void mouseClicked(MouseEvent m){
                fr.dispose();
            }
        }
      );
      submit.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent a){
                msg.setText("WELCOME!!");
            }        
        }


      );


        fr.addKeyListener(
            new KeyAdapter(){
                public void keyPressed(KeyEvent e){
                    fr.dispose();
                }
            }
        );
    }


    public static void main(String[] args) {
        Ui obj = new Ui();
       
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
    }




}
