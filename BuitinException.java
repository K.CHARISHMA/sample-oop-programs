import java.util.InputMismatchException;
import java.util.Scanner;
public class Exception{
   public static void main(String[] args){
       Scanner input = new Scanner(System.in);
       System.out.println("Enter 2 numbers : ");
       try{
       int num1 = input.nextInt();
       int num2 = input.nextInt();
       int result= num1/num2;
       System.out.println("the result is "+ result);
       }catch(ArithmeticException obj){
           System.out.println("Denominator should not be zero");
       }catch(InputMismatchException obj){
           obj.printStackTrace();
           System.out.println("Input should be integers");
       }
   }


}
