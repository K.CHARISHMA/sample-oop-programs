#include<iostream>
using namespace std;
class child1{
    public:
        int a=10;
        void display1(){
            cout<<"content in child1 is "<<a<<endl;
        }
};
//simple inheritance
class child2 : public child1{
    public:
        int b=20;
        int c=a+b;
        void display2(){
            cout<<"content in child2 is "<<c<<endl;
        }


};
//multi level inheritance
class child3 : public child2{
    public:
        int d=30;
        int e=c+d;
        void display3(){
            cout<<"content in child3 is "<<e<<endl;


        }
};
//hierarchical inheritance
class child4 : public child1{
    public:
        int f=40;
        void display4(){
            cout<<"content in child4 is "<<a+f<<endl;
        }
};
//hybrid inheritance
class child5 : public child2{
    public:
        int g=50;
        void display5(){
            cout<<"content in child5 is "<<b+g<<endl;
        }
};
int main(){
    child2 obj1;
    obj1.display2();
    child3 obj2;
    obj2.display3();
    child4 obj3;
    obj3.display4();
    child5 obj4;
    obj4.display5();


}
