public class Mutable{
     public static void main(String[] args) {
        int a= 10;
        String name= "Cherry";
        StringBuffer obj =new StringBuffer("Charishma");
        System.out.println("int before "+ System.identityHashCode(a));
        a=20;
        System.out.println("int after "+ System.identityHashCode(a));
        System.out.println("String before "+ System.identityHashCode(name));
        name = name.concat(" joy");
        System.out.println("System after "+ System.identityHashCode(name));
        System.out.println("StringBuffer before "+ System.identityHashCode(obj));
        obj.append(" Rao");
        System.out.println("StringBuffer after "+ System.identityHashCode(obj));
    }
}
