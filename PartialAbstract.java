import java.lang.*;
abstract class Animal{
    abstract void eat();
    void sleep(){
        System.out.println("Animals need 8hrs of sleep");
    }
}
class PartialAbstract extends Animal{
    void eat(){
        System.out.println("Animals eat 3 times a day");
    }
    public static void main(String[] args) {
        PartialAbstract obj=new PartialAbstract();
        obj.eat();
        obj.sleep();    
    }
}


