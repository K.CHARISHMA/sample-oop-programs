import java.lang.*;
import java.util.*;


class Threads implements Runnable{
    int n1;
    Threads(int n1){
        this.n1=n1;
    }
        public void run(){
            multiply(n1);
        }
        void multiply(int num){
            for(int i=0;i<=12;i++){
                System.out.println(num +" * "+i+" = "+num*i);
            }
        }
}
class ThreadInterface{
    public static void main(String[] args) {
        Threads t1 =new Threads(3);
        Threads t2 = new Threads(5);
        t1.run();
        t2.run();
       
    }
}
