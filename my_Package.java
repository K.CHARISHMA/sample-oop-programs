package myPackage;
public class fibOfN {
public void fibonocciSeries(int num){
        int num1 = 0, num2 = 1;
        int counter = 0;
        while (counter < num) {
            System.out.print(num1 + " ");
            int num3 = num2 + num1;
            num1 = num2;
            num2 = num3;
            counter = counter + 1;
        }
        System.out.print( "\n ");
}
public int nThFib(int n){
    if (n==0||n==1){
        return 0;
}
    else if(n==2){
        return 1;
}
    return nThFib(n - 1) + nThFib(n - 2);
}
}
