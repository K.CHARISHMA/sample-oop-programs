#include<iostream>
using namespace std;
class pureAbstract{
   public:
       virtual void teacher()=0;
       virtual void student()=0;
};
class child : public pureAbstract{
   public:
       void teacher(){
           cout<<"Good mrng students"<<endl;
       }
       void student(){
           cout<<"Good mrng Sir!!"<<endl;
       }
};
int main(){
   child obj;
   obj.teacher();
   obj.student();
}
